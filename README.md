# e3 IOC cookiecutter template

[Cookiecutter](https://github.com/audreyr/cookiecutter) template for e3 IOCs.

Once you have finished setting up your IOC, make sure to remove all template comments as well as empty files and directories.

> Note: This is a template for use with a deployment system currently under development. It is based on the current needs of that
> system and is subject to change at any point. Caveat pistorum, as they say.

## Prerequisites

- Python3
- Cookiecutter

## Quickstart

Generate an e3 IOC:

```
$ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc.git
```

As this is not easy to remember, you can add an alias in your `~/.bash_profile`:

```
alias e3-ioc='cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc.git'
```

## Modules

The startup script loads the standard ESS EPICS modules using the [e3-essioc](https://gitlab.esss.lu.se/e3/wrappers/core/e3-essioc.git) package:

```
iocshLoad("$(essioc_DIR)/common_config.iocsh")
```

The **e3-essioc** package includes:
* iocStats
* recsync
* caPutLog
* autosave

Those modules shall not be defined in the `st.cmd` file.

The modules required by the IOC shall be added to the startup script (without version):

```
require mymodule
```
